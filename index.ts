import { HttpMethods, HttpRequest, HttpResponse, HttpStatus } from 'http4ts/src/core/http';
import { HttpFilter, HttpHandler } from 'http4ts/src/core/http4ts';


const includes = <T extends unknown>(
	element: T,
	elements: T[]
): boolean => elements.includes(element);

const join = (
	separator: string|undefined,
	elements: string[]
): string => elements.join(separator);

export type Origin = string/* | RegExp*/;

export type Config = {
	origins: Origin[]
	methods: HttpMethods[]
	preflightContinue: boolean
	optionsSuccessStatus: number
	controlRequestHeaders?: string[]
	controlAllowCredentials: boolean
	controlExposedHeaders?: string[]
	controlMaxAge?: number
}

export const config = (): Config => ({
	origins: ['*'],
	methods: [
		HttpMethods.GET,
		HttpMethods.HEAD,
		HttpMethods.PUT,
		HttpMethods.PATCH,
		HttpMethods.DELETE
	],
	preflightContinue: false,
	optionsSuccessStatus: 204,
	controlAllowCredentials: false
});

export const withOrigins = (config: Config, origins: Origin[]) => ({
	...config,
	origins
});

const isOriginAllowed = (allowedOrigins: Origin[], origin: string) =>
	includes(origin, allowedOrigins)

const prepareSetHeader = (response: HttpResponse) =>
	(key: string, value: string) => {
		if( typeof response.headers[key] !== 'undefined' )
		{
			//TODO: log that we're overriding the header
		}
		response.headers[key] = value;
	};

type SynchronousHttpFilter = (handler: HttpHandler) => HttpHandler

const httpAccessControlAllowOriginFilter = (config: Config): SynchronousHttpFilter =>
	(handler: HttpHandler):  HttpHandler =>
		async (request: HttpRequest): Promise<HttpResponse> => {
			let response = await handler(request);
			const setHeader = prepareSetHeader(response);
			const origins = config.origins;

			if( includes('*',origins) ) //TODO: optimize?
			{
				setHeader('access-control-allow-origin', '*');
			}
			else if(
				origins.length === 1 &&
				typeof origins[0] === 'string'
			)
			{
				setHeader('access-control-allow-origin', origins[0]);
			}
			else if(
				typeof request.headers.origin !== 'undefined' &&
				isOriginAllowed(origins, request.headers.origin)
			)
			{
				setHeader('access-control-allow-origin', request.headers.origin);
				setHeader('vary', 'Origin');
			}
			return response;
		}

const httpAccessControlAllowMethodsFilter = (config: Config): SynchronousHttpFilter =>
	(handler: HttpHandler):  HttpHandler =>
		async (request: HttpRequest): Promise<HttpResponse> => {
			let response = await handler(request);
			const setHeader = prepareSetHeader(response);

			setHeader('access-control-allow-methods', config.methods.join());

			return response;
		}

const httpAccessControlAllowCredentialsFilter = (config: Config): SynchronousHttpFilter =>
	(handler: HttpHandler):  HttpHandler =>
		async (request: HttpRequest): Promise<HttpResponse> => {
			let response = await handler(request);
			const setHeader = prepareSetHeader(response);

			if( config.controlAllowCredentials )
			{
				setHeader('access-control-allow-credentials', 'true');
			}

			return response;
		}

const httpAccessControlAllowedRequestHeadersFilter = (config: Config): SynchronousHttpFilter =>
	(handler: HttpHandler):  HttpHandler =>
		async (request: HttpRequest): Promise<HttpResponse> => {
			let response = await handler(request);
			const setHeader = prepareSetHeader(response);
			const headers = config.controlRequestHeaders;

			if(
				typeof headers === 'undefined'
			)
			{
				if(
					typeof request.headers['access-control-request-headers']
					!==
					'undefined'
				)
				{
					setHeader(
						'Access-Control-Allow-Headers',
						request.headers['access-control-request-headers']
					);
					setHeader('vary', 'Access-Control-Allow-Headers');
				}
			}
			else
			{
				setHeader(
					'Access-Control-Allow-Headers',
					join(',', headers)
				);
			}

			return response;
		}

const httpAccessControlExposedHeadersFilter = (config: Config): SynchronousHttpFilter =>
	(handler: HttpHandler):  HttpHandler =>
		async (request: HttpRequest): Promise<HttpResponse> => {
			let response = await handler(request);
			const setHeader = prepareSetHeader(response);
			const headers = config.controlExposedHeaders;

			if( typeof headers !== 'undefined' )
			{
				setHeader('Access-Control-Expose-Headers', join(',', headers));
			}

			return response;
		}

const httpAccessControlMaxAgeFilter = (config: Config): SynchronousHttpFilter =>
	(handler: HttpHandler):  HttpHandler =>
		async (request: HttpRequest): Promise<HttpResponse> => {
			let response = await handler(request);
			const setHeader = prepareSetHeader(response);
			const maxAge = config.controlMaxAge;

			if( typeof maxAge !== 'undefined' )
			{
				setHeader('Access-Control-Max-Age', maxAge.toString());
			}

			return response;
		}

export const httpCorsFilter = (config: Config): SynchronousHttpFilter => {
	const origin = httpAccessControlAllowOriginFilter(config);
	const credentials = httpAccessControlAllowCredentialsFilter(config);
	const methods = httpAccessControlAllowMethodsFilter(config);
	const allowedHeaders = httpAccessControlAllowedRequestHeadersFilter(config);
	const maxAge = httpAccessControlMaxAgeFilter(config);
	const exposedHeaders = httpAccessControlExposedHeadersFilter(config);

	return (handler: HttpHandler): HttpHandler => {
		let preflightHandler = handler;
		preflightHandler = origin(preflightHandler);
		preflightHandler = credentials(preflightHandler);
		preflightHandler = methods(preflightHandler);
		preflightHandler = allowedHeaders(preflightHandler);
		preflightHandler = maxAge(preflightHandler);
		preflightHandler = exposedHeaders(preflightHandler);

		let standardHandler = handler;
		standardHandler = origin(standardHandler);
		standardHandler = credentials(standardHandler);
		standardHandler = exposedHeaders(standardHandler);

		return async (request: HttpRequest): Promise<HttpResponse> => {
			let ret: HttpResponse|Promise<HttpResponse>;
			if( request.method === HttpMethods.OPTIONS )
			{
				ret = await preflightHandler(request);

				ret.status = HttpStatus.NO_CONTENT;
				ret.headers['content-length'] = '0';
			}
			else
			{
				ret = await standardHandler(request);
			}
			return ret;
		}
	};
}
